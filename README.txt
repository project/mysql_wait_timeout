This module  intends to address  the quite common  "MySQL has gone  away" error.
Many  shared, production  MySQL  servers  set a  rather  low  value for  MySQL's
wait_timeout  parameter (e.g.,  1  minute) and  this is  to  be considered  good
practice  as most  connections to  such servers  are intended  to generate  HTTP
responses as fast as possible.

On the other hand, Drupal may keep  its MySQL connection idle for long durations
for valuable purposes, such as running its  cron job or running a Drush command:
content import, link checking, reindexation, etc.

The mysql_wait_timeout module is able to enforce wait_timeout in up to three
different, optional contexts:
 * default: wait_timeout is enforced during hook_init
 * drush: wait_timeout is enforced during hook_init if Drush is detected
 * cron: wait_timeout is enforced among the first invoked hook_cron()s.

However, Drupal 7 does not need a complete module to set MySQL's wait_timeout:
customizing "init_commands" when declaring the databases in settings.php is
enough. That's why mysql_wait_timeout for Drupal 7 is not a real module but a
simple file to include in settings.php.

The following variables are to be enforced in settings.php:
 * mysql_wait_timeout_enforce_default: TRUE to systematically enforce
   wait_timeout to mysql_wait_timeout_default_duration (in seconds)
 * mysql_wait_timeout_enforce_cron: TRUE to enforce wait_timeout to
   mysql_wait_timeout_cron_duration (in seconds) for cronjobs
 * mysql_wait_timeout_enforce_drush: TRUE to enforce wait_timeout to
   mysql_wait_timeout_drush_duration (in seconds) for Drush commands

After having declared those variables in the $conf array, include the
mysql_wait_timeout.inc.php file, e.g.:
  @incude 'sites/all/modules/mysql_wait_timeout/mysql_wait_timeout.inc.php';
