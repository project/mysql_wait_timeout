<?php
/**
 * @file
 * Configuration include file provided by mysql_wait_timeout.
 *
 * Setting wait_timeout for Drupal 7 does not require a complete module; this
 * file is simply to be included in settings.php, after having declared the
 * adequate mysql_wait_timeout_* values (see README.txt for details).
 *
 * Note: this "module" only deals with the "default/default" database entry.
 * This will probably be improved later.
 */

// Do nothing if $databases is empty; indeed, systematically filling $databases
// breaks Drupal installations (e.g. via drush site-install) because
// includes/core.install.inc expects both $GLOBALS['databases'] and
// $GLOBALS['db_url'] to be empty.
// @codingStandardsIgnoreStart
if (empty($databases)) {
// @codingStandardsIgnoreEnd
  return 1;
}

// Do nothing if the default database is not a MySQL database.
// @codingStandardsIgnoreStart
if (!preg_match('/mysql/', $databases['default']['default']['driver'])) {
// @codingStandardsIgnoreEnd
  return 1;
}

// Initialize $conf, just in case.
// @codingStandardsIgnoreStart
$conf = isset($conf) ? $conf : array();
// @codingStandardsIgnoreEnd

// By default, do not enforce MySQL's wait_timeout.
$mysql_wait_timeout = NULL;

// Check mysql_wait_timeout_* variables to decide whether and how to enforce
// MySQL's wait_timeout.
if (isset($conf['mysql_wait_timeout_enforce_default']) && $conf['mysql_wait_timeout_enforce_default']) {
  $mysql_wait_timeout = $conf['mysql_wait_timeout_default_duration'];
}
if (isset($conf['mysql_wait_timeout_enforce_drush']) && $conf['mysql_wait_timeout_enforce_drush']) {
  if (function_exists('drush_main')) {
    $mysql_wait_timeout = $conf['mysql_wait_timeout_drush_duration'];
  }
}
if (isset($conf['mysql_wait_timeout_enforce_cron']) && $conf['mysql_wait_timeout_enforce_cron']) {
  if (function_exists('drush_main')) {
    // Ok, we run through Drush... but do we run through drush core-cron?
    foreach ($_SERVER['argv'] as $arg) {
      if (preg_match('/^(core-)?cron$/', $arg)) {
        $mysql_wait_timeout = $conf['mysql_wait_timeout_cron_duration'];
      }
    }
  }
  elseif (isset($_SERVER['SCRIPT_FILENAME']) && preg_match('/(^|\/)cron\.php$/', $_SERVER['SCRIPT_FILENAME'])) {
    $mysql_wait_timeout = $conf['mysql_wait_timeout_cron_duration'];
  }
}

// Enforce MySQL's wait_timeout for the default database.
if (!is_null($mysql_wait_timeout)) {
  if (preg_match('/^[0-9]+$/', $mysql_wait_timeout)) {
    $databases['default']['default']['init_commands'] = array(
      'SET SESSION wait_timeout = ' . $mysql_wait_timeout,
    );
  }
}
